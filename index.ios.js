import { AppRegistry}  from 'react-native';
import App from './components/App';

AppRegistry.registerComponent('Switcher', () => App);
