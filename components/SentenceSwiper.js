import React, { Component } from 'react';
import { Platform, Dimensions, StyleSheet, Text, View } from 'react-native';
import SentencePage from './SentencePage';
import Swiper from 'react-native-swiper';

var {height, width} = Dimensions.get('window');
var buttonListHeight = (Platform.OS === 'ios') ? 70 : 94;
var swiperHeight = height - buttonListHeight;

export default class SentencePages extends Component {
  render() {
    let problem = this.props.problem;

    return (
      <Swiper loop={false} height={swiperHeight} onMomentumScrollEnd={this.props.onMomentumScrollEnd} dot={<Dot active={false} />} activeDot={<Dot active={true} />}>
        {
          problem.sentences.map((item, key) => {
            let prevKorean = key > 0 ? problem.sentences[key-1].korean : null;
            let prevEnglish = key > 0 ? problem.sentences[key-1].english : null;

            return (
              <SentencePage key={key} english={item.english} korean={item.korean} prevKorean={prevKorean} prevEnglish={prevEnglish} problemId={problem.id}/>
            );
          })
        }
      </Swiper>
    );
  }
}

class Dot extends Component {
  render() {
    let activeStyle = this.props.active ? styles.activeDot : {};

    return (
      <View style={[styles.dot, activeStyle]}>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  dot: {
    height: 12,
    width: 12,
    borderRadius: 15,
    backgroundColor: 'lightgray',
    marginRight: 10,
  },
  activeDot: {
    backgroundColor: '#FECF2F',
  },
  line: {
    width: 10,
    height: 1,
    backgroundColor: 'black',
  }
})
