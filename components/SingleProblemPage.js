import React, { Component } from 'react';
import { Platform, Dimensions, Image, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Swiper from 'react-native-swiper';
import { default as Sound } from 'react-native-sound';

import db from './realm/RealmMigration';
import SentenceSwiper from './SentenceSwiper';
import LastPage from './LastPage';
import TipModal from './TipModal';

var {height, width} = Dimensions.get('window');
var buttonListHeight = (Platform.OS === 'ios') ? 70 : 94;
var swiperHeight = height - buttonListHeight;

export default class SingleProblemPage extends Component {
  constructor(props) {
    super(props);
    problem = this.props.problem;
    this.state = {
      sentence: problem.sentences[0],
      displayTip: false,
      tipSeen: false,
    };
    this._onMomentumScrollEnd = this._onMomentumScrollEnd.bind(this);
    this._displayTip = this._displayTip.bind(this);
    this._playSound = this._playSound.bind(this);
  }

  _onMomentumScrollEnd(e, state, context) {
    this.setState({
      sentence: this.props.problem.sentences[state.index],
      tipSeen: false,
    });
  }

  _displayTip() {
    this.setState({
      displayTip: !this.state.displayTip,
      tipSeen: true, 
    });
  }

  _playSound() {
    var filename = 'sentence' + (this.state.sentence.id + 1) + '.m4a';
    
    var s = new Sound(filename, Sound.MAIN_BUNDLE, (e) => {
      if (e) {
        console.log('error', filename);
      } else {
        this.setState({soundPlaying: true});
        s.play((success) => { this.setState({soundPlaying: false}); });
      }
    })
  }

  render() {
    let tip = this.state.displayTip ? <TipModal title={"Tip!"} text={this.state.sentence.tip} onPress={this._displayTip}/> : null;
    var tipPresent = this.state.sentence && this.state.sentence.tip
    var tipImage = !tipPresent ? require('./img/tip_empty.png') : require('./img/tip.png')

    return (
      <View style={styles.container}>
        <View style={{height: swiperHeight}}>
          <SentenceSwiper problem={this.props.problem} onMomentumScrollEnd={this._onMomentumScrollEnd}/>
        </View>
        <View style={styles.buttonList}>
          <Button text={"Tip"} source={tipImage} active={this.state.displayTip} activeSource={require('./img/tip_filled.png')} onPress={this._displayTip} disabled={!tipPresent}/>
          <Button text={"Sound"} source={require('./img/sound.png')} active={this.state.soundPlaying} activeSource={require('./img/sound_filled.png')} onPress={this._playSound} disabled={this.state.soundPlaying}/>
        </View>
        {tip}
      </View>
    );
  }
}

class Button extends Component {
  render() {
    let imageSource = this.props.active ? this.props.activeSource : this.props.source;
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.button} disabled={this.props.disabled}>
        <Image source={imageSource} style={styles.buttonImage}/>
        <Text style={styles.buttonText}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}
Button.defaultProps = {disabled: false};


var additionalPadding = (Platform.OS === 'ios') ? 11 : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonList: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopColor: 'lightgray',
    borderTopWidth: 1,
  },
  button: {
    alignItems: 'center',
  },
  buttonImage: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  buttonText: {
    fontSize: 10,
    color: 'black',
  },
  tip: {
    top: 43 + additionalPadding + 40,
    position: 'absolute',
    height: 200,
    backgroundColor: 'white',
  },
  tipText: {
    fontSize: 50,
    color: 'black',
  },
});
