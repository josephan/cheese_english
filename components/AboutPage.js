import React, { Component } from 'react';
import { Linking, StyleSheet, Text, View } from 'react-native';

export default class AboutPage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.instructions}>
          😊 치즈잉글리쉬를 이용해주셔서 감사합니다. 😊{"\n"}
          Thank you for downloading this app.
        </Text>
        <Text style={styles.instructions}>
          치즈잉글리쉬는 한 단계씩 문장을 길게 말하는 방법을 터득하면서 영어가 입에 붙는 맛을 느끼게 하는 어플리케이션입니다.{"\n"}
        </Text>
        <Text style={styles.instructions}>
          앞으로도 계속되는 업그레이드를 통해 더 많은 컨텐츠를 이용하실 수 있도록 노력하겠습니다.{"\n"}
        </Text>
        <Text style={styles.instructions}>
          앱 이용 중에 문제가 있거나 더 좋은 방향으로 제안 사항이 있으시면 cheese@joyley.co로 이메일을 보내주시면 감사하겠습니다.{"\n"}
          If you have any issues with the app or have suggestions to make the app better please email cheese@joyley.co
        </Text>
        <Text style={styles.instructions}>
          Last Updated: November 6, 2016
        </Text>
        <Text style={{color: 'blue'}}
          onPress={() => Linking.openURL('https://www.joyley.co')}>
          https://www.joyley.co/
        </Text>
        <Text>
          Version: 1.2
        </Text>
        <Text>
          Made by: 안정현 & 심해원
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
    padding: 30,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    color: '#333333',
    marginBottom: 10,
  },
});
