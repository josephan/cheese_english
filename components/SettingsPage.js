import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import db from './realm/RealmMigration';

export default class SettingsPage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Sorry!
        </Text>
        <Text style={styles.instructions}>
          I don't have anything to show here yet.
        </Text>
        <Image source={require('./img/penguinsorry.png')} style={{height: 230, width: 175,}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 30,
  },
});
