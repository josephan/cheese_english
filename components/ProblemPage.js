import React, { Component } from 'react';
import { AsyncStorage, Platform, Dimensions, Image, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { default as Sound } from 'react-native-sound';

import db from './realm/RealmMigration';
import SentenceSwiper from './SentenceSwiper';
import LastPage from './LastPage';
import TipModal from './TipModal';

var {height, width} = Dimensions.get('window');
var buttonListHeight = (Platform.OS === 'ios') ? 70 : 94;
var swiperHeight = height - buttonListHeight;

export default class ProblemPage extends Component {
  constructor(props) {
    super(props);
    let problem = db.objects('Problem').filtered('completed == false')[0];
    if (problem == null) {
      this.state = {
        pages: () => <LastPage nextProblem={this._nextProblem}/>,
        problem: null,
        viewedLastPage: false,
        completed: false,
        sentence: null,
        displayTip: false,
        soundPlaying: false,
      }
    } else {
      this.state = {
        pages: () => <SentenceSwiper problem={problem} onMomentumScrollEnd={this._onMomentumScrollEnd}/>,
        problem: problem,
        viewedLastPage: false,
        completed: false,
        sentence: problem.sentences[0],
        displayTip: false,
        soundPlaying: false,
      };
      db.write(() => {problem.seen = true});
    }
    this._onMomentumScrollEnd = this._onMomentumScrollEnd.bind(this);
    this._nextProblem = this._nextProblem.bind(this);
    this._prevProblem = this._prevProblem.bind(this);
    this._displayTip = this._displayTip.bind(this);
    this._playSound = this._playSound.bind(this);
  }

  _onMomentumScrollEnd(e, state, context) {
    this.setState({
      sentence: this.state.problem.sentences[state.index],
    });
    if (state.index === state.total - 1) {
      this.setState({viewedLastPage: true});
    }
  }

  _displayTip() {
    this.setState({
      displayTip: !this.state.displayTip,
    });
  }

  _prevProblem() {
    var currentProblem = this.state.problem;

    if (currentProblem == null) {
      var problem = db.objects('Problem').slice(-1)[0];
    } else {
      var currentIndex = currentProblem.id;
      if (currentIndex === 0) { return; }
      var problem = db.objectForPrimaryKey('Problem', currentIndex - 1);
    }

    this.setState({
      pages: () => <SentenceSwiper problem={problem} onMomentumScrollEnd={this._onMomentumScrollEnd} />,
      problem: problem,
      viewedLastPage: false,
      sentence: problem.sentences[0],
      completed: false,
      displayTip: false,
      soundPlaying: false,
    })
    Actions.refresh({title: problem.id + 1 + ". " + problem.name});
  }

  _nextProblem() {
    if (this.state.problem && !this.state.problem.completed) {
      db.write(() => {
        this.state.problem.completed = true;
      });
    }

    if (this.state.problem == null) {
      var currentIndex = -1;
    } else {
      var currentIndex = this.state.problem.id;
    }
    var problem = db.objectForPrimaryKey('Problem', currentIndex + 1);

    if (problem == null) {
      this.setState({
        pages: () => <LastPage nextProblem={this._nextProblem}/>,
        problem: null,
        viewedLastPage: false,
        completed: true,
        sentence: null,
        displayTip: false,
        soundPlaying: false,
      })
      Actions.refresh({title: 'Complete!'});
    } else {
      this.setState({
        pages: () => <SentenceSwiper problem={problem} onMomentumScrollEnd={this._onMomentumScrollEnd} />,
        problem: problem,
        viewedLastPage: false,
        sentence: problem.sentences[0],
        completed: false,
        displayTip: false,
        soundPlaying: false,
      });
      db.write(() => {problem.seen = true});
      Actions.refresh({title: problem.id + 1 + ". " + problem.name});
    }
  }

  _playSound() {
    var filename = 'sentence' + (this.state.sentence.id + 1) + '.m4a';
    
    var s = new Sound(filename, Sound.MAIN_BUNDLE, (e) => {
      if (e) {
        console.log('error', e);
      } else {
        this.setState({soundPlaying: true});
        s.play((success) => { this.setState({soundPlaying: false}); });
      }
    })
  }

  render() {
    const Pages = this.state.pages;
    let tip = this.state.displayTip ? <TipModal title={"Tip!"} text={this.state.sentence.tip} onPress={this._displayTip}/> : null;
    let tipPresent = this.state.sentence && this.state.sentence.tip
    let tipImage = !tipPresent ? require('./img/tip_empty.png') : require('./img/tip.png')

    return (
      <View style={styles.container}>
        <View style={{height: swiperHeight}}>
          <Pages />
        </View>
        <View style={styles.buttonList}>
          <Button text={"Previous"} source={require('./img/prev.png')} active={false} activeSource={'not needed'} onPress={this._prevProblem} disabled={false}/>
          <Button text={"Tip"} source={tipImage} active={this.state.displayTip} activeSource={require('./img/tip_filled.png')} onPress={this._displayTip} disabled={!tipPresent}/>
          <Button text={"Sound"} source={require('./img/sound.png')} active={this.state.soundPlaying} activeSource={require('./img/sound_filled.png')} onPress={this._playSound} disabled={!this.state.problem || this.state.soundPlaying}/>
          <Button text={"Next Lesson"} source={require('./img/next.png')} active={this.state.viewedLastPage} activeSource={require('./img/next_filled.png')} onPress={this._nextProblem} disabled={!this.state.viewedLastPage || this.state.problem == null}/>
        </View>
        {tip}
      </View>
    );
  }
}

class Button extends Component {
  render() {
    let imageSource = this.props.active ? this.props.activeSource : this.props.source;
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.button} disabled={this.props.disabled}>
        <Image source={imageSource} style={styles.buttonImage}/>
        <Text style={styles.buttonText}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}
Button.defaultProps = {disabled: false};


var additionalPadding = (Platform.OS === 'ios') ? 11 : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonList: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopColor: '#828287',
    borderTopWidth: StyleSheet.hairlineWidth,
  },
  button: {
    alignItems: 'center',
  },
  buttonImage: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  buttonText: {
    fontSize: 10,
    color: 'black',
  },
  tip: {
    top: 43 + additionalPadding + 40,
    position: 'absolute',
    height: 200,
    backgroundColor: 'white',
  },
  tipText: {
    fontSize: 50,
    color: 'black',
  },
});
