import React, { Component } from 'react';
import { Image, Platform, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

// [non_flipped_color, flipped_color]
const COLORS = [
  ['#FFB032', '#FFE2B5'], // orange
  ['#84A8FD', '#C3D4FF'], // blue
  ['#FF9673', '#FFD8DC'], // red
  ['#7CD778', '#C0F6BD'], // green
]

export default class SentencePage extends Component {
  constructor(props) {
    super(props);
    this._higlightWords = this._higlightWords.bind(this);
  }

  _clean(array, deleteValue) {
    for (var i = 0; i < array.length; i++) {
      if (array[i] === deleteValue) {
        array.splice(i, 1);
        i--;
      }
    }
    return array;
  }

  _splitString(string) {
    return this._clean(string.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"").split(" "), "");
  }

  _indexDiffs(previousString, currentString) {
    if (previousString === null) { return []; }

    var previousStringArray = this._splitString(previousString.toLowerCase());
    var currentStringArray = this._splitString(currentString.toLowerCase());
    var newWordIndexes = [];
    var i = 0;

    for (var x = 0; x < currentStringArray.length; x++) {
      var comparisonBool = previousStringArray[x-i] === currentStringArray[x];
      if (!comparisonBool) {
        newWordIndexes.push(x);
        i++;
      }
    }

    return newWordIndexes;
  }

  _higlightWords(string, highlightDiffs) {
    return string.split(" ").map((word, index) => {
      if (highlightDiffs.includes(index)) {
        return (
          <Text key={index}>
            <Text style={styles.highlighted}>{word}</Text><Text>{' '}</Text>
          </Text>
        );
      } else {
        return (
          <Text key={index}>{word}{' '}</Text>
        );
      }
    });
  }
  
  render() {
    let koreanDiffIndex = this._indexDiffs(this.props.prevKorean, this.props.korean);
    let englishDiffIndex = this._indexDiffs(this.props.prevEnglish, this.props.english);
    return (
      <View style={styles.container}>
        <View style={styles.cardContainer}>
          <KoreanCard text={this._higlightWords(this.props.korean, koreanDiffIndex)} />
          <EnglishCard text={this._higlightWords(this.props.english, englishDiffIndex)} problemId={this.props.problemId} />
        </View>
      </View>
    );
  }
}

class KoreanCard extends Component {
  render() {
    return (
      <View style={[styles.card, styles.koreanCard]}>
        <Text style={styles.text}>{this.props.text}</Text>
      </View>
    );
  }
}

class EnglishCard extends Component {
  constructor(props) {
    super(props);
    this.state = {showText: false};
    this._toggleText = this._toggleText.bind(this);
  }

  componentWillMount() {
    this.setState({showText: false});
  }

  _toggleText() {
    this.setState({showText: !this.state.showText});
  }

  render() {
    let text = this.state.showText ? this.props.text : ' '
    let colors = COLORS[this.props.problemId % 4] 
    let active = this.state.showText ? {backgroundColor: colors[1]} : {};
    let englishCard = {backgroundColor: colors[0]};
    let image = this.state.showText ?  null : <Instructions />

    return (
      <TouchableOpacity style={[styles.card, englishCard, active]} onPress={this._toggleText}>
        <Text style={styles.text}>{text}</Text>
        {image}
      </TouchableOpacity>
    );
  }
}

class Instructions extends Component {
  render() {
    return (
      <View style={styles.instruction}>
        <Image source={require('./img/press.png')} style={{height: 60, width: 60,}} />
      </View>
    );
  }
}

var additionalPadding = (Platform.OS === 'ios') ? 11 : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 73 + additionalPadding,
  },
  cardContainer: {
    flex: 1,
    marginBottom: 50,
  },
  card: {
    flex: 1,
    backgroundColor: '#EBEBEB',
    padding: 20,
    borderRadius: 5,
  },
  koreanCard: {
    marginBottom: 20,
  },
  text: {
    color: '#555',
    fontSize: 21
  },
  highlighted: {
    color: 'black',
    fontWeight: 'bold',
  },
  instruction: {
    paddingTop: 40,
    alignItems: 'center',
  }
});
