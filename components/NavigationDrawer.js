import React, { Component, PropTypes } from 'react';
import { Dimensions, Image, Platform, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Actions, DefaultRenderer } from 'react-native-router-flux';
import Drawer from 'react-native-drawer';
import db from './realm/RealmMigration';

export default class extends Component {
    render(){
        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
              open={state.open}
              onOpen={()=>Actions.refresh({key:state.key, open: true})}
              onClose={()=>Actions.refresh({key:state.key, open: false})}
              type="overlay"
              content={<SideMenu />}
              tapToClose={true}
              openDrawerOffset={0.35}
              panCloseMask={0.35}
              styles={drawerStyles}
              negotiatePan={true}
              tweenHandler={(ratio) => ({
               main: { opacity:Math.max(0.54,1-ratio) }
            })}>
            <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
            </Drawer>
        );
    }
}

const SideMenu = (props, context) => {
  const drawer = context.drawer;
  let iosPadding = (Platform.OS === 'ios') ? <View style={styles.iosPadding}></View> : null;

  return (
    <View style={styles.container}>
      {iosPadding}
      <View style={styles.header}>
        <Image source={require('./img/penguinpizza.png')} style={styles.headerImage}/>
      </View>
      <PageLink
        source={require('./img/problems.png')}
        text={"학습중 | Learning"}
        onPress={() => { drawer.close(); Actions.problemPage(); }}
      />
      <PageLink
        source={require('./img/timeline.png')}
        text={"타임라인 | Timeline"}
        onPress={() => { drawer.close(); Actions.historyPage(); }}
      />
      <PageLink
        source={require('./img/settings.png')}
        text={"설정 | Settings"}
        onPress={() => { drawer.close(); Actions.settingsPage(); }}
      />
      <PageLink
        source={require('./img/home.png')}
        text={"회사소개 | About"}
        onPress={() => { drawer.close(); Actions.aboutPage(); }}
      />
    </View>
  );
};

SideMenu.contextTypes = {
  drawer: React.PropTypes.object,
};

SideMenu.PropTypes = {
  name: PropTypes.string,
  sceneStyle: View.propTypes.style,
  title: PropTypes.string,
};

class PageLink extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.pageLink}>
        <Image source={this.props.source} style={styles.pageLinkImage}/>
        <Text style={styles.pageLinkText}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

const drawerStyles = {
  drawer: {
    borderRightColor: '#828287',
    borderRightWidth: StyleSheet.hairlineWidth,
  },
  main: {
    backgroundColor: 'black',
  },
}

var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  iosPadding: {
    height: 20,
    backgroundColor: 'white',
  },
  header: {
    borderBottomColor: '#777D85',
    borderBottomWidth: 0.5,
  },
  headerImage: {
    height: 240,
    width: width - (width * 0.35) - 1,
  },
  pageLink: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 60,
    paddingLeft: 25,
    borderBottomColor: '#828287',
    borderBottomWidth: 0.5,
  },
  pageLinkImage: {
    width: 30,
    height: 30,
    marginRight: 25,
  },
  pageLinkText: {
    color: 'black',
    fontSize: 16,
    fontWeight: '300',
  },
});
