import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { Scene, Router } from 'react-native-router-flux';

import NavigationDrawer from './NavigationDrawer';
import ProblemPage from './ProblemPage';
import HistoryPage from './HistoryPage';
import SettingsPage from './SettingsPage';
import AboutPage from './AboutPage';
import SingleProblemPage from './SingleProblemPage';
import SadPenguin from './SadPenguin';

import db from './realm/RealmMigration';

class Switcher extends Component {
  _getLastOpened() {
    return db.objects('Setting')[0].lastOpened;
  }

  _setLastOpened() {
    db.write(() => {
      let settings = db.objects('Setting')[0];
      settings.lastOpened = new Date();
    })
  }

  _openedLongTimeAgo () {
    const currentTime = new Date().getTime();
    const lastOpened = this._getLastOpened().getTime();
    const oneDay = 86400000;
    var lastOpenedSince =  Math.round((currentTime - lastOpened) / oneDay);
    console.log('DATA!: ', lastOpenedSince);
    return lastOpenedSince >= 7
  }

  render() {
    let openedOverSevenDays = this._openedLongTimeAgo();
    this._setLastOpened();
    let problem = db.objects('Problem').filtered('completed == false')[0];
    let title = problem == null || problem.length == 0 ? "Complete!" : problem.id + 1 + ". " + problem.name;

    return (
      <Router>
        <Scene key="root">
          <Scene key="drawer" component={NavigationDrawer} open={false}>
            <Scene key="main" tabs={false} navigationBarStyle={styles.navBar} titleStyle={styles.title}>
              <Scene key="sadPenguin"
                component={SadPenguin}
                title={"I thought we were friends?"}
                drawerImage={require('./img/menu.png')}
                hideNavBar={true}
                initial={openedOverSevenDays} />
              <Scene key="problemPage"
                component={ProblemPage}
                title={title}
                type="replace"
                drawerImage={require('./img/menu.png')}
                hideNavBar={false}
                initial={!openedOverSevenDays} />
              <Scene key="historyPage"
                component={HistoryPage}
                backButtonImage={require('./img/back_arrow.png')}
                title="타임라인" />
              <Scene key="settingsPage"
                component={SettingsPage}
                backButtonImage={require('./img/back_arrow.png')}
                title="설정" />
              <Scene key="aboutPage"
                component={AboutPage}
                backButtonImage={require('./img/back_arrow.png')}
                title="회사소개" />
              <Scene key="singleProblemPage"
                component={SingleProblemPage}
                backButtonImage={require('./img/back_arrow.png')}
                title="Single Problem Page" />
            </Scene>
          </Scene>
        </Scene>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  navBar: {
    backgroundColor: 'white',
  },
  title: {
    color: 'rgb(255, 149, 0)',
    fontWeight: '500',
  }
});

export default Switcher
