import Realm from 'realm';
import json1 from './data/json1';
import json2 from './data/json2';

import schema1 from './schemas/schema1.js'
import schema2 from './schemas/schema2.js'
import schema3 from './schemas/schema3.js'


var schemas = [
  schema1,
  schema2,
  schema3,
]

let schemaVersion = Realm.schemaVersion(Realm.defaultPath);

function createNewObjects(realm, json, problemId, sentenceId) {
  var problemId = problemId;
  var sentenceId = sentenceId;
  realm.write(() => {
    for (var i = 0; i < json.length; i++) {
      let problem = json[i];
      let realmProblem = realm.create('Problem', {id: problemId, name: problem.name});
      problemId++;

      for (var j = 0; j < problem.sentences.length; j++) {
        let sentence = problem.sentences[j];
        let realmSentence = realm.create('Sentence', {id: sentenceId, english: sentence.english, korean: sentence.korean, tip: sentence.tip});

        realmProblem.sentences.push(realmSentence);
        sentenceId++;
      }
    }
  });
}

function createSetting(realm, setting) {
  realm.write(() => {
    let realmSetting = realm.create('Setting', {setting})
  })
}

var tempRealm;
// v1 when the app is initially downloaded
if (schemaVersion === -1) {
  tempRealm = new Realm(schemas[1]);
  createNewObjects(tempRealm, json1, 0, 0);
  tempRealm.close();
  schemaVersion = 2;
} 

// v1.1 by deleting all previous records
// and creating new ones
if (schemaVersion === 1) {
  tempRealm = new Realm(schemas[0]);
  tempRealm.write(() => {
    tempRealm.deleteAll();
  });
  tempRealm.close();
  tempRealm = new Realm(schemas[1]);
  createNewObjects(tempRealm, json1, 0, 0);
  tempRealm.close();
  schemaVersion = 2;
}

// v1.2 adding new sentences
if (schemaVersion === 2) {
  let totalProblems = json1.length;
  tempRealm = new Realm(schemas[2]);
  createNewObjects(tempRealm, json2, totalProblems, totalProblems * 4);
  createSetting(tempRealm, {lastOpened: new Date()})
  schemaVersion = 3;
}

var finalRealm = new Realm(schemas[2])

export default finalRealm;
