var SentenceSchema = {
  name: 'Sentence',
  properties: {
    english: 'string',
    korean: 'string',
    tip: {type: 'string', optional: true}
  }
}
var ProblemSchema = {
  name: 'Problem',
  properties: {
    name: 'string',
    sentences: {type: 'list', objectType: 'Sentence'},
    completed: {type: 'bool', default: false},
    seen: {type: 'bool', default: false},
    bookmark: {type: 'bool', default: false},
    bookmarkedOn: {type: 'date', optional: true}
  }
}

export default { schema: [SentenceSchema, ProblemSchema], schemaVersion: 1 }
