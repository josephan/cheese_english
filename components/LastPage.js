import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import db from './realm/RealmMigration';

export default class LastPage extends Component {
  _startOver() {
    let completedProblems = db.objects('Problem').filtered('completed == true').snapshot();
    db.write(() => {
      for (let i = 0, len = completedProblems.length; i < len; i++) {
        completedProblems[i].completed = false;
      }
    });
    this.props.nextProblem();
  }

  render() {
    return (
      <View style={styles.lastPage}>
        <Text style={styles.text}>Uh oh... I'm sorry but I ran out of sentences.</Text>
        <Text style={styles.text}>It takes some time for me to think of new sentences.</Text>
        <Text style={styles.text}>Thanks for being so patient!</Text>
        <Image source={require('./img/sad_penguin.png')} style={{height: 200, width: 256.4, marginTop: 60}}/>
        <TouchableOpacity onPress={this._startOver.bind(this)} style={styles.button}>
          <Text style={styles.buttonText}>Start Again!</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  lastPage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: 'black',
  },
  buttonText: {
    fontSize: 16,
    color: 'black',
  },
  button: {
    height: 40,
    width: 100,
    backgroundColor: '#FECF2F',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 20,
    borderWidth: 2,
    borderColor: 'black',
  },
});

