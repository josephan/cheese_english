import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class SadPenguin extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('./img/angrypenguin.png')} style={styles.penguin} />
        <TouchableOpacity style={styles.button} onPress={() => {Actions.problemPage();}}>
          <Text style={styles.buttonText}>Ok! Sorry...</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F1F1F1',
  },
  penguin: {
    height: 400,
    resizeMode: 'contain',
  },
  button: {
    height: 40,
    width: 130,
    backgroundColor: '#FECF2F',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 40,
    borderWidth: 2,
    borderColor: 'black',
  },
  buttonText: {
    color: 'black'
  }
});
