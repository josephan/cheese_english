import React, { Component } from 'react';
import { Platform, Dimensions, Image, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, Text, View } from 'react-native';


export default class TipModal extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity activeOpacity={1} style={styles.background} onPress={this.props.onPress}>
        </TouchableOpacity>
        <View style={styles.tipBox}>
          <Text style={styles.tipTitle}>{this.props.title}</Text>
          <Text style={styles.tipText}>{this.props.text}</Text>
          <TouchableOpacity onPress={this.props.onPress}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>OK</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

var {height, width} = Dimensions.get('window');
var additionalPadding = (Platform.OS === 'ios') ? 11 : 0;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    top: 0,
    height: height,
    width: width,
    padding: 35,
    paddingTop: 100 + additionalPadding,
    paddingBottom: 110,
    zIndex: 1,
  },
  background: {
    position: 'absolute',
    top: 0,
    height: height,
    width: width,
  },
  tipBox: {
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 15,
    paddingTop: 10,
    alignItems: 'center',
  },
  button: {
    height: 40,
    width: 180,
    borderRadius: 100,
    backgroundColor: '#62CCF0',
    overflow: 'hidden',
    marginTop: 10,
    justifyContent: 'center',
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white',
  },
  tipTitle: {
    color: 'black',
    fontSize: 25,
    textAlign: 'center',
    marginBottom: 10,
  },
  tipText: {
    fontSize: 20,
    color: 'black',
  },
});
