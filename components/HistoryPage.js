import React, { Component } from 'react';
import { Image, Platform, TouchableOpacity, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import db from './realm/RealmMigration';

export default class HistoryPage extends Component {
  render() {
    let problem = db.objects('Problem');
    let extraSpace = (Platform.OS === 'android') ? <View style={styles.extraSpace}></View> : null;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          {problem.map((item, key) => {
            return (
              <ProblemLink text={item.id + 1 + '. ' + item.name} key={key} seen={item.seen} onPress={() => Actions.singleProblemPage({title: item.name, problem: item})} last={key + 1 == problem.length}/>
            );
          })}
          <View style={{height: 50,}}></View>
          {extraSpace}
        </ScrollView>
        <Image source={require('./img/penguinscroll1.png')} style={styles.penguin}/>
      </View>
    );
  }
}

class ProblemLink extends Component {
  render() {
    let completedCircle = this.props.seen ? styles.completedCircle : {};
    let completedText = this.props.seen ? styles.completedText : {};
    let lastline = this.props.last ? null : <View style={styles.line}></View>;
    return (
      <View style={styles.problem}>
        <TouchableOpacity style={styles.problemButton} onPress={this.props.onPress} disabled={!this.props.seen}>
          <View style={[styles.circle, completedCircle]}></View>
          <Text style={[styles.problemText, completedText]}>{this.props.text}</Text>
        </TouchableOpacity>
        {lastline}
      </View>
    );
  }
}

var additionalPadding = (Platform.OS === 'ios') ? 11 : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    paddingTop: 113 + additionalPadding,
  },
  problem: {
    marginLeft: 70,
  },
  problemButton: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  circle: {
    height: 25,
    width: 25,
    borderRadius: 25,
    borderWidth: 0.5,
    borderColor: '#979797',
  },
  completedCircle: {
    backgroundColor: '#F7D454',
  },
  completedText: {
    color: 'black',
  },
  problemText: {
    marginLeft: 20,
    color: 'gray',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  line: {
    height: 50,
    width: 0.5,
    backgroundColor: '#979797',
    marginLeft: 12.5,
  },
  penguin: {
    height: 172.5,
    width: 206.5,
    position: 'absolute',
    bottom: -60,
    right: -60,
    transform: [{rotate: '-20deg'}],
  },
  extraSpace: {
    height: 110,
  },
});
